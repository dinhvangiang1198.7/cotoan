import React, { Component } from 'react';
import './App.css'

class App extends Component {

  state = {
    banco: null,
    nguoiChoi: true,
    ocoDangChon: null
  }

  componentDidMount() {
    const banco = [];
    const hang = [null, null, null, null, null, null, null, null, null];
    for (let i = 0; i < 11; i++) {
      banco.push([...hang]);
    }
    for (let i = 1; i < 10; i++) {
      banco[0][i-1] = {
        red: false,
        number: 10 - i
      }
    }
    banco[1][4] = {
      red: false,
      number: 0
    }
    for (let i = 1; i < 10; i++) {
      banco[10][i-1] = {
        red: true,
        number: i
      }
    }
    banco[9][4] = {
      red: true,
      number: 0
    }
    this.setState({
      banco
    });
    console.log(banco);
  }

  onClickNguoiChoi = (x,y) => {
    console.log("chon", x,y);
    if (this.state.banco[y][x].number === 0) return;
    this.setState({
      ocoDangChon: {
        x: x,
        y: y,
        red: true
      }
    });
  }

  chonOTrong = (x,y) => {
    const {ocoDangChon, banco} = this.state;
    console.log("click",x,y);
    if (ocoDangChon) {
      if (ocoDangChon.red) {
        if (this.nguoiChoiCoTheDi(ocoDangChon.x, ocoDangChon.y, x, y)) {
          const stateMoi = []
          this.state.banco.map(el => {
            stateMoi.push([...el]);
            return null;
          })
          console.log(this.state.banco[ocoDangChon.y][ocoDangChon.x], ocoDangChon);
          stateMoi[y].splice(x, 1, {
            ...this.state.banco[ocoDangChon.y][ocoDangChon.x]
          })
          stateMoi[ocoDangChon.y][ocoDangChon.x] = null;
          console.log(this.state.banco[ocoDangChon.y][ocoDangChon.x]);
          this.setState({
            banco: stateMoi
          })
        }
      }
    }
  }

  nguoiChoiCoTheDi = (x,y, nextx, nexty) => {
    if (x === nextx || y == nexty || nextx-x === nexty-y) return true;
    if (!this.state.banco[nexty][nextx]) return false;
    return false;
  }

  render() {
    const { banco } = this.state;
    console.log(banco);
    if (!banco) return null;
    return (
      <table>
        <tbody>
          {
            banco.map((el, y) => {
              return (
                <tr key={y}>
                  {
                    el.map((oco, x) => {
                      if (!oco) return <td key={x} onClick={() => this.chonOTrong(x,y)}></td>;
                      if (oco.red) {
                        return (
                          <td key={x} className='red'
                            onClick={() => this.onClickNguoiChoi(x,y)}>
                            <div className='oco'>
                              {oco.number}
                            </div>
                          </td>
                        )
                      }
                      return (
                        <td key={x} className='black'
                        onClick={() => this.onClickNguoiChoi(x,y)}>
                          <div className='oco'>
                            {oco.number}
                          </div>
                        </td>
                      )
                    })
                  }
                </tr>
              )
            })
          }
        </tbody>
      </table>
    )
  }
}

export default App;
